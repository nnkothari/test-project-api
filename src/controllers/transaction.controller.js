import { TransactionRepository } from '../modules/db/repository/transactionRepository';
import { getResponse, getErrorResponse } from '../modules/shared/config/response';

const httpStatus = require('http-status');

export class TransactionController {
    constructor() { }

    // Create new transaction entry (debit/credit)
    async createTransaction(details) {
        try {
            const transactionRepository = new TransactionRepository();
            const response = await transactionRepository.createTransaction(details);
            return getResponse(httpStatus.OK, response);
        } catch (error) {
            return getErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, error);
        }
    }

    // Get transaction information from Id
    async getTransactionByCustomerid(customerId) {
        try {
            const transactionRepository = new TransactionRepository();
            const response = await transactionRepository.getTransactionByCustomerid(customerId);
            return getResponse(httpStatus.OK, response);
        } catch (error) {
            return getErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, error);
        }
    }

    // Create transfer transaction
    async createTransfer(details) {
        try {
            const transactionRepository = new TransactionRepository();
            const response = await transactionRepository.createTransfer(details);
            return getResponse(httpStatus.OK, response);
        } catch (error) {
            return getErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, error);
        }
    }
}