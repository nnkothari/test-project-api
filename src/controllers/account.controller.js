import { AccountRepository } from '../modules/db/repository/accountRepository';
import { getResponse, getErrorResponse } from '../modules/shared/config/response';

const httpStatus = require('http-status');

export class AccountController {
    constructor() { }

    // Creates new account
    async createAccount(account_obj) {
        try {
            const accountRepository = new AccountRepository();
            const response = await accountRepository.createAccount(account_obj);
            return getResponse(httpStatus.OK, response);
        } catch (error) {
            return getErrorResponse(httpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get account data from Id
    async getAccountById(accountId) {
        try {
            const accountRepository = new AccountRepository();
            const response = await accountRepository.getAccountById(accountId);
            return getResponse(httpStatus.OK, response);
        } catch (error) {
            return getErrorResponse(httpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update account data
    async updateAccount(account_obj) {
        try {
            const accountRepository = new AccountRepository();
            const response = await accountRepository.updateAccount(account_obj);
            return getResponse(httpStatus.OK, response);
        } catch (error) {
            return getErrorResponse(httpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}