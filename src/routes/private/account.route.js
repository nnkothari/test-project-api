import { AccountController } from '../../controllers/account.controller';

const validate = require('express-validation');

const account = (router) => {

    // route for Creates new account
    router.post(
        '/createaccount',
        async (req, res, next) => {
            const accountController = new AccountController();
            const response = await accountController.createAccount(req.body);
            return res.status(response.status).send(response);
        }
    );

    // route for  Get account data from Id
    router.get(
        '/getaccountid',
        async (req, res, next) => {
            const accountController = new AccountController();
            const response = await accountController.getAccountById(req.query.accountId);
            return res.status(response.status).send(response);
        }
    );

    // route for  Update account data
    router.post(
        '/updateaccount',
        async (req, res, next) => {
            const accountController = new AccountController();
            const response = await accountController.updateAccount(req.body);
            return res.status(response.status).send(response);
        }
    );
}

export default account;